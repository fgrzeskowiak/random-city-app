package com.filip.randomcityapp.dagger

import android.content.Context
import android.location.Geocoder
import dagger.Module
import dagger.Provides
import java.util.*

@Module
object ApplicationModule {

    @Provides
    fun geocoder(context: Context) = Geocoder(context, Locale.getDefault())
}