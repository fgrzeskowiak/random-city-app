package com.filip.randomcityapp.dagger

import android.content.Context
import com.filip.randomcityapp.repository.LifecycleNotifier
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        SchedulersModule::class,
        DatabaseModule::class,
        DatabaseBindingModule::class
    ]
)
interface ApplicationComponent {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): ApplicationComponent
    }

    val viewModelsComponent: ViewModelsComponent
    val lifecycleNotifier: LifecycleNotifier
}