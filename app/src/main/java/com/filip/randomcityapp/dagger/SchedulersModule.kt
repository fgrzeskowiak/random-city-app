package com.filip.randomcityapp.dagger

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
object SchedulersModule {

    @Provides
    @Singleton
    @UiScheduler
    fun provideUiScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @NetworkScheduler
    fun provideNetworkScheduler(): Scheduler = Schedulers.io()

    @Provides
    @Singleton
    @ComputationScheduler
    fun provideComputationScheduler(): Scheduler = Schedulers.computation()
}