package com.filip.randomcityapp.dagger

import com.filip.randomcityapp.views.main.MainViewModel
import com.filip.randomcityapp.views.splash.SplashViewModel
import dagger.Subcomponent

@Subcomponent
interface ViewModelsComponent {
    val splashViewModel: SplashViewModel
    val mainViewModel: MainViewModel
}