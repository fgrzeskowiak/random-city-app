package com.filip.randomcityapp.dagger

import android.content.Context
import androidx.room.Room
import com.filip.randomcityapp.db.CityWithColorDatabaseDao
import com.filip.randomcityapp.db.DeliveryDatabaseImpl
import com.filip.randomcityapp.db.RandomCityDatabase
import com.filip.randomcityapp.db.RandomCityDb
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DatabaseBindingModule {
    @Binds
    abstract fun randomCityDatabase(randomCityDatabaseImpl: DeliveryDatabaseImpl): RandomCityDatabase
}

@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun roomMessagesDatabase(context: Context): RandomCityDb =
        Room.databaseBuilder(context, RandomCityDb::class.java, "random_city_db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun cityWithColorDatabaseDao(db: RandomCityDb): CityWithColorDatabaseDao = db.cityWithColorDatabaseDao()
}