package com.filip.randomcityapp.repository

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LifecycleNotifier @Inject constructor() : LifecycleObserver {

    private val lifecycleEventSubject = PublishSubject.create<LifecycleEvent>()
    val lifecycleEvents: Observable<LifecycleEvent> = lifecycleEventSubject

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun toForeground() {
        lifecycleEventSubject.onNext(LifecycleEvent.Foreground)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun toBackground() {
        lifecycleEventSubject.onNext(LifecycleEvent.Background)
    }
}

sealed class LifecycleEvent {
    object Foreground : LifecycleEvent()
    object Background : LifecycleEvent()
}