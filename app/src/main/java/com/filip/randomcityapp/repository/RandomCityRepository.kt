package com.filip.randomcityapp.repository

import com.filip.randomcityapp.common.refresh
import com.filip.randomcityapp.dagger.ComputationScheduler
import com.filip.randomcityapp.db.RandomCityDatabase
import io.reactivex.Observable
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RandomCityRepository @Inject constructor(
    randomCityDatabase: RandomCityDatabase,
    lifecycleNotifier: LifecycleNotifier,
    @ComputationScheduler computationScheduler: Scheduler
) {

    private val cities =
        listOf("Gdańsk", "Warszawa", "Poznań", "Białystok", "Wrocław", "Katowice", "Kraków")

    private val colors = listOf("Yellow", "Green", "Blue", "Red", "Black", "White")

    private val refreshObservable = lifecycleNotifier.lifecycleEvents
        .filter { it == LifecycleEvent.Foreground }
        .skip(1)
        .map { Unit }

    val randomCity: Observable<RandomCity> =
        Observable.interval(5, TimeUnit.SECONDS, computationScheduler)
            .map { RandomCity(cities.random(), colors.random(), System.currentTimeMillis()) }
            .doOnNext { randomCityDatabase.putCity(it) }
            .takeUntil(lifecycleNotifier.lifecycleEvents.filter { it == LifecycleEvent.Background })
            .refresh(refreshObservable)
            .replay()
            .autoConnect()
}

data class RandomCity(val city: String, val color: String, val timestamp: Long)