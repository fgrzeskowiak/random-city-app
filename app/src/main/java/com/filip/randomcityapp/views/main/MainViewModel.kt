package com.filip.randomcityapp.views.main

import android.graphics.Color
import androidx.lifecycle.ViewModel
import arrow.core.Option
import arrow.core.getOrElse
import com.filip.randomcityapp.common.BaseAdapterItem
import com.filip.randomcityapp.common.KotlinBaseAdapterItem
import com.filip.randomcityapp.dagger.UiScheduler
import com.filip.randomcityapp.parsers.ColorParser
import com.filip.randomcityapp.parsers.DateParser
import com.filip.randomcityapp.repository.RandomCityRepository
import com.filip.randomcityapp.views.main.details.CityCoordsProvider
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MainViewModel @Inject constructor(
    repository: RandomCityRepository,
    @UiScheduler uiScheduler: Scheduler,
    cityCoordsProvider: CityCoordsProvider
) : ViewModel() {

    private val citySelectedSubject = PublishSubject.create<CityItem>()
    private val backClickedSubject = PublishSubject.create<Unit>()
    private val selectedCityCoordsSubject = BehaviorSubject.create<Option<LatLng>>()
    private val selectedItemTimestampSubject = BehaviorSubject.create<Long>()
    private val toolbarUpdateSubject = BehaviorSubject.create<ToolbarUpdate>()

    init {
        citySelectedSubject
            .switchMap { cityCoordsProvider.getCityCoords(it.city) }
            .subscribe(selectedCityCoordsSubject)

        citySelectedSubject
            .map { it.timestamp }
            .mergeWith(backClickedSubject.map { -1L })
            .subscribe(selectedItemTimestampSubject)

        citySelectedSubject
            .map<ToolbarUpdate> { ToolbarUpdate.CityColor(it.city, it.color) }
            .mergeWith(backClickedSubject.map { ToolbarUpdate.Default })
            .subscribe(toolbarUpdateSubject)
    }

    val cityItems: Observable<List<BaseAdapterItem>> = repository
        .randomCity
        .map { randomCity ->
            CityItem(
                randomCity.timestamp,
                randomCity.city,
                ColorParser.parseColorName(randomCity.color).getOrElse { Color.BLACK },
                DateParser.convertMillisToDate(randomCity.timestamp),
                citySelectedSubject,
                selectedItemTimestampSubject
                    .map { it == randomCity.timestamp }
                    .startWith(false)
            )
        }
        .scan(emptyList<BaseAdapterItem>()) { list, new ->
            list.plus(new).sortedWith(
                compareBy(
                    { (it as? CityItem)?.city },
                    { (it as? CityItem)?.timestamp }
                )
            )
        }
        .observeOn(uiScheduler)

    val withMap: (Observable<GoogleMap>) -> Observable<CityDetailsState> = { mapObservable ->
        selectedCityCoordsSubject
            .observeOn(uiScheduler)
            .switchMap { coords ->
                mapObservable
                    .map { map ->
                        coords.fold(
                            { CityDetailsState.Error },
                            { CityDetailsState.Loaded(map, it) }
                        )
                    }
                    .startWith(CityDetailsState.Loading)
            }
    }

    val openCity: Observable<CityItem> = citySelectedSubject
        .distinctUntilChanged()

    val toolbarUpdate: Observable<ToolbarUpdate> = toolbarUpdateSubject

    fun backClicked() {
        backClickedSubject.onNext(Unit)
    }
}

sealed class ToolbarUpdate {
    data class CityColor(val city: String, val color: Int) : ToolbarUpdate()
    object Default : ToolbarUpdate()
}

sealed class CityDetailsState {
    data class Loaded(val map: GoogleMap, val latLng: LatLng) : CityDetailsState()
    object Error : CityDetailsState()
    object Loading : CityDetailsState()
}

data class CityItem(
    val timestamp: Long,
    val city: String,
    val color: Int,
    val date: String,
    val citySelectedObserver: Observer<CityItem>,
    val selectedItemObservable: Observable<Boolean>
) : KotlinBaseAdapterItem<Long> {
    override fun itemId(): Long = timestamp

    fun citySelected() {
        citySelectedObserver.onNext(this)
    }
}