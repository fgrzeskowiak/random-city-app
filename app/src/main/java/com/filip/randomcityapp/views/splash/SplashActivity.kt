package com.filip.randomcityapp.views.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.filip.randomcityapp.R
import com.filip.randomcityapp.common.startActivity
import com.filip.randomcityapp.dagger.viewModel
import com.filip.randomcityapp.injector
import com.filip.randomcityapp.views.main.MainActivity
import io.reactivex.disposables.CompositeDisposable

class SplashActivity : AppCompatActivity() {
    private val viewModel: SplashViewModel by viewModel { injector.splashViewModel }
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onStart() {
        super.onStart()
        disposable.add(
            viewModel.randomCityGenerated
                .subscribe {
                    startActivity<MainActivity>()
                    finish()
                }
        )
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}