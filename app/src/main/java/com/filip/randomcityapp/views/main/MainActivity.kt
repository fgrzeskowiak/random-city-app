package com.filip.randomcityapp.views.main

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.transition.Slide
import com.filip.randomcityapp.R
import com.filip.randomcityapp.common.addFragment
import com.filip.randomcityapp.common.checkPlayServices
import com.filip.randomcityapp.common.replaceFragment
import com.filip.randomcityapp.dagger.viewModel
import com.filip.randomcityapp.injector
import com.filip.randomcityapp.parsers.ColorParser
import com.filip.randomcityapp.views.main.details.CityDetailsFragment
import com.filip.randomcityapp.views.main.list.CitiesFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val STATE_TOOLBAR_COLOR = "toolbarColor"
        private const val STATE_TOOLBAR_TITLE = "toolbarText"
        private const val TAG_FRAGMENT_CITIES = "cities"
        private const val TAG_FRAGMENT_CITY_DETAILS = "city_details"
        private const val PLAY_SERVICES_REQUEST_CODE = 2
    }

    private val disposable = CompositeDisposable()
    private val viewModel: MainViewModel by viewModel { injector.mainViewModel }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPlayServices(PLAY_SERVICES_REQUEST_CODE)

        when {
            savedInstanceState == null ->
                addFragment(
                    R.id.main_fragment_container,
                    createCitiesFragment(),
                    TAG_FRAGMENT_CITIES
                )
            resources.getBoolean(R.bool.isTablet) -> handleTabletOrientationChange()
        }
    }

    override fun onStart() {
        super.onStart()
        disposable.addAll(
            viewModel.openCity
                .subscribe { openDetailsFragment() },
            viewModel.toolbarUpdate
                .subscribe(::updateToolbar)
        )
    }

    private fun updateToolbar(toolbarUpdate: ToolbarUpdate) {
        when (toolbarUpdate) {
            is ToolbarUpdate.CityColor -> toolbarByCity(toolbarUpdate.city, toolbarUpdate.color)
            ToolbarUpdate.Default -> defaultToolbar()
        }
    }

    private fun toolbarByCity(city: String, color: Int) {
        with(main_toolbar) {
            setBackgroundColor(color)
            setTitleTextColor(if (ColorParser.isColorTooBright(color)) Color.BLACK else Color.WHITE)
            title = city
        }

    }

    private fun defaultToolbar() {
        with(main_toolbar) {
            setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))
            setTitleTextColor(Color.WHITE)
            title = getString(R.string.app_name)
        }
    }

    private fun openDetailsFragment() {
        if (detail_fragment_container != null) {
            openDetailsInTheSecondContainer()
        } else {
            openDetailsInTheMainContainer()
        }
    }

    private fun handleTabletOrientationChange() {
        if (currentMainFragment() is CityDetailsFragment) {
            openCitesInTheMainContainer() //Changing from portrait to landscape
        } else {
            if (supportFragmentManager.findFragmentByTag(TAG_FRAGMENT_CITY_DETAILS) != null &&
                detail_fragment_container == null
            ) {
                openDetailsInTheMainContainer() // Changing from landscape to portrait
            }
        }
    }

    /** Fragments navigation */

    private fun openCitesInTheMainContainer() {
        replaceFragment(
            R.id.main_fragment_container,
            createCitiesFragment(),
            TAG_FRAGMENT_CITIES
        )
        if (supportFragmentManager.findFragmentById(R.id.detail_fragment_container) == null) {
            replaceFragment(
                R.id.detail_fragment_container,
                createDetailsFragment(),
                TAG_FRAGMENT_CITY_DETAILS
            )
        }
    }

    private fun openDetailsInTheMainContainer() {
        replaceFragment(
            R.id.main_fragment_container,
            createDetailsFragment().apply {
                enterTransition = Slide(Gravity.END)
                exitTransition = Slide(Gravity.END)
            },
            TAG_FRAGMENT_CITY_DETAILS,
            addToBackStack = true
        )
    }

    private fun openDetailsInTheSecondContainer() {
        replaceFragment(
            R.id.detail_fragment_container,
            createDetailsFragment(),
            TAG_FRAGMENT_CITY_DETAILS
        )
    }

    private fun currentMainFragment() =
        supportFragmentManager.findFragmentById(R.id.main_fragment_container)

    /** It is impossible to change fragment's container ID while it's alive,
     *  so details fragment must be constructed every time it's needed
     */
    private fun createDetailsFragment() = CityDetailsFragment()

    private fun createCitiesFragment() = supportFragmentManager
        .findFragmentByTag(TAG_FRAGMENT_CITIES) as? CitiesFragment
        ?: CitiesFragment().apply {
            enterTransition = Slide(Gravity.START)
            exitTransition = Slide(Gravity.START)
        }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }

    override fun onBackPressed() {
        viewModel.backClicked()
        super.onBackPressed()
    }
}