package com.filip.randomcityapp.views.splash

import androidx.lifecycle.ViewModel
import com.filip.randomcityapp.dagger.UiScheduler
import com.filip.randomcityapp.repository.RandomCityRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    randomRepository: RandomCityRepository,
    @UiScheduler uiScheduler: Scheduler
) : ViewModel() {

    val randomCityGenerated: Observable<*> = randomRepository
        .randomCity
        .take(1)
        .observeOn(uiScheduler)
}