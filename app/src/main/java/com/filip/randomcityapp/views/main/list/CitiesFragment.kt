package com.filip.randomcityapp.views.main.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.filip.randomcityapp.R
import com.filip.randomcityapp.common.BaseAdapter
import com.filip.randomcityapp.dagger.activityViewModel
import com.filip.randomcityapp.injector
import com.filip.randomcityapp.views.main.MainViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_cities.*

class CitiesFragment : Fragment(R.layout.fragment_cities) {

    private val disposable = CompositeDisposable()
    private val baseAdapter by lazy {
        BaseAdapter(listOf(CityViewHolderManager()))
    }
    private val viewModel: MainViewModel by activityViewModel { injector.mainViewModel }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        cities_recycler_view.apply {
            layoutManager = LinearLayoutManager(context).apply {
                recycleChildrenOnDetach = true
            }
            addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
            adapter = baseAdapter
            adapter?.stateRestorationPolicy =
                RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        }
    }

    override fun onStart() {
        super.onStart()
        disposable.addAll(
            viewModel.cityItems
                .subscribe(baseAdapter::submitList)
        )
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
    }
}