package com.filip.randomcityapp.views.main.details;

import android.location.Address;
import android.location.Geocoder;

import com.filip.randomcityapp.dagger.NetworkScheduler;
import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;
import javax.inject.Singleton;

import arrow.core.Option;
import arrow.core.Some;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

@Singleton
public class CityCoordsProvider {
    private final Geocoder geocoder;
    private final Scheduler networkScheduler;

    @Inject
    public CityCoordsProvider(Geocoder geocoder, @NetworkScheduler Scheduler networkScheduler) {
        this.geocoder = geocoder;
        this.networkScheduler = networkScheduler;
    }

    public Observable<Option<LatLng>> getCityCoords(String cityName) {
        return Observable.<Option<LatLng>>create(emitter -> {
            try {
                Address cityAddress = geocoder.getFromLocationName(cityName, 1).get(0);
                LatLng latLng = new LatLng(cityAddress.getLatitude(), cityAddress.getLongitude());
                emitter.onNext(new Some<>(latLng));
            } catch (Exception e) {
                emitter.onNext(Option.Companion.empty());
            }
        }).subscribeOn(networkScheduler);
    }
}
