package com.filip.randomcityapp.views.main.list

import android.content.res.Configuration
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.filip.randomcityapp.R
import com.filip.randomcityapp.common.BaseAdapterItem
import com.filip.randomcityapp.common.ViewHolderManager
import com.filip.randomcityapp.parsers.ColorParser
import com.filip.randomcityapp.views.main.CityItem
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.item_city.view.*

class CityViewHolderManager : ViewHolderManager {
    override fun matches(baseAdapterItem: BaseAdapterItem) = baseAdapterItem is CityItem

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater) =
        ViewHolder(inflater.inflate(R.layout.item_city, parent, false))

    inner class ViewHolder(itemView: View) : ViewHolderManager.BaseViewHolder<CityItem>(itemView) {

        private val disposable = CompositeDisposable()

        override fun bind(item: CityItem) {
            with(itemView) {
                item_city_name.text = item.city
                item_city_name.setTextColor(item.color)
                item_city_date.text = item.date
                item_city_name.setShadowLayer(
                    if (ColorParser.isColorTooBright(item.color)) 0.05f else 0f,
                    1f,
                    1f,
                    Color.GRAY
                )
                setOnClickListener { item.citySelected() }
                disposable.add(
                    item.selectedItemObservable
                        .subscribe { itemView.item_city_layout.isSelected = it }
                )
            }
        }

        override fun onViewRecycled() {
            disposable.clear()
            itemView.setOnClickListener(null)
        }
    }
}