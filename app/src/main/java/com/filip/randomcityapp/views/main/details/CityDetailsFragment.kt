package com.filip.randomcityapp.views.main.details

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.filip.randomcityapp.R
import com.filip.randomcityapp.dagger.activityViewModel
import com.filip.randomcityapp.injector
import com.filip.randomcityapp.views.main.CityDetailsState
import com.filip.randomcityapp.views.main.MainViewModel
import com.filip.randomcityapp.views.main.details.map.mapObservable
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_city_details.*

class CityDetailsFragment : Fragment(R.layout.fragment_city_details) {

    private val disposable = CompositeDisposable()
    private val viewModel: MainViewModel by activityViewModel { injector.mainViewModel }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        map_view.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        disposable.add(
            viewModel.withMap(map_view.mapObservable())
                .subscribe {
                    when (it) {
                        is CityDetailsState.Loaded -> moveMap(it.map, it.latLng)
                        CityDetailsState.Loading -> showProgress()
                        CityDetailsState.Error -> showError()
                    }
                }
        )
    }

    private fun showError() {
        map_view.isVisible = false
        map_progress.isVisible = false
        Snackbar.make(requireView(), getString(R.string.map_error), Snackbar.LENGTH_LONG).show()
    }

    private fun showProgress() {
        map_progress.isVisible = true
    }

    private fun moveMap(map: GoogleMap, latLng: LatLng) {
        map_progress.isVisible = false
        map.addMarker(MarkerOptions().position(latLng))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14f))
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onStop() {
        super.onStop()
        disposable.clear()
        map_view.onStop()
    }
}