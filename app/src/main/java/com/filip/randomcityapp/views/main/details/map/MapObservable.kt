package com.filip.randomcityapp.views.main.details.map

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import com.filip.randomcityapp.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.MapStyleOptions
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.MainThreadDisposable

fun MapView.mapObservable(): Observable<GoogleMap> = MapObservable(this)

class MapObservable(private val mapView: MapView) : Observable<GoogleMap>() {
    override fun subscribeActual(observer: Observer<in GoogleMap>?) {
        val disposable = Disposable(observer, this.mapView.context)
        observer?.onSubscribe(disposable)
        mapView.getMapAsync(disposable)
    }

    private class Disposable(
        private val observer: Observer<in GoogleMap>?,
        private val context: Context
    ) : MainThreadDisposable(), OnMapReadyCallback {
        override fun onMapReady(googleMap: GoogleMap?) {
            googleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            if (context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES) {
                googleMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_night_style))
            }
            if (!isDisposed && googleMap != null) observer?.onNext(googleMap)
        }

        override fun onDispose() {
            // Do nothing
        }
    }
}