package com.filip.randomcityapp

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment
import androidx.lifecycle.ProcessLifecycleOwner
import com.filip.randomcityapp.dagger.ApplicationComponent
import com.filip.randomcityapp.dagger.DaggerApplicationComponent

class MainApplication : Application(), ComponentProvider {

    override val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.factory()
            .create(applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(component.lifecycleNotifier)
    }
}

interface ComponentProvider {
    val component: ApplicationComponent
}

val Activity.injector get() = (application as ComponentProvider).component.viewModelsComponent
val Fragment.injector get() = (activity!!.application as ComponentProvider).component.viewModelsComponent
