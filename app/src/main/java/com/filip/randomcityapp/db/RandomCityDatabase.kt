package com.filip.randomcityapp.db

import androidx.room.*
import com.filip.randomcityapp.repository.RandomCity
import io.reactivex.Flowable
import javax.inject.Inject

@Database(entities = [RandomCityEntity::class], version = 1)
abstract class RandomCityDb : RoomDatabase() {
    abstract fun cityWithColorDatabaseDao(): CityWithColorDatabaseDao

    override fun clearAllTables() {
        cityWithColorDatabaseDao().clearDb()
    }
}

interface RandomCityDatabase {
    fun putCity(randomCity: RandomCity)
    fun getCity(timestamp: Long): Flowable<RandomCity>
    fun getAllCities(): Flowable<List<RandomCity>>
    fun clear()
}

@Entity(tableName = "citiesWithColors", primaryKeys = ["timestamp"])
data class RandomCityEntity(
    val timestamp: Long,
    val city: String,
    val color: String
) {
    fun toRandomCity() = RandomCity(city, color, timestamp)
}

private fun RandomCity.toEntity(): RandomCityEntity = RandomCityEntity(timestamp, city, color)

@Dao
interface CityWithColorDatabaseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun put(cityWithColorEntity: RandomCityEntity)

    @Query("SELECT * FROM citiesWithColors WHERE timestamp = :timestamp")
    fun get(timestamp: Long): Flowable<RandomCityEntity>

    @Query("SELECT * FROM citiesWithColors")
    fun getAll(): Flowable<List<RandomCityEntity>>

    @Query("DELETE FROM citiesWithColors")
    fun clearDb()
}

class DeliveryDatabaseImpl @Inject constructor(private val db: CityWithColorDatabaseDao) :
    RandomCityDatabase {
    override fun putCity(randomCity: RandomCity) = db.put(randomCity.toEntity())

    override fun getCity(timestamp: Long): Flowable<RandomCity> =
        db.get(timestamp).map { it.toRandomCity() }

    override fun getAllCities(): Flowable<List<RandomCity>> =
        db.getAll().map { it.map { entity -> entity.toRandomCity() } }

    override fun clear() {
        db.clearDb()
    }


}