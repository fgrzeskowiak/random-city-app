package com.filip.randomcityapp.common

import io.reactivex.Observable

fun <T> Observable<T>.refresh(refresher: Observable<Unit>): Observable<T> = compose { upstream ->
    refresher
        .startWith(Unit)
        .switchMap { upstream }
}