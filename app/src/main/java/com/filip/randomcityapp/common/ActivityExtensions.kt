package com.filip.randomcityapp.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

inline fun <reified T : Activity> Context.startActivity() =
    startActivity(Intent(this, T::class.java))

fun FragmentActivity.replaceFragment(
    @IdRes container: Int,
    fragment: Fragment,
    tag: String?,
    addToBackStack: Boolean = false
) {
    supportFragmentManager.beginTransaction()
        .replace(container, fragment, tag)
        .apply { if (addToBackStack) addToBackStack(tag) }
        .commit()
}

fun FragmentActivity.addFragment(
    @IdRes container: Int,
    fragment: Fragment,
    tag: String?
) {
    supportFragmentManager.beginTransaction()
        .add(container, fragment, tag)
        .commit()
}

fun Activity.checkPlayServices(requestCode: Int) {
    with(GoogleApiAvailability.getInstance()) {
        isGooglePlayServicesAvailable(this@checkPlayServices).let { status ->
            if (status != ConnectionResult.SUCCESS) {
                if (isUserResolvableError(status)) {
                    getErrorDialog(this@checkPlayServices, status, requestCode).show()
                }
            }
        }
    }
}