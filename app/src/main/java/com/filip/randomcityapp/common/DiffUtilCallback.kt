package com.filip.randomcityapp.common

import androidx.recyclerview.widget.DiffUtil

class DiffUtilCallback : DiffUtil.ItemCallback<BaseAdapterItem>() {
    override fun areItemsTheSame(
        oldItem: BaseAdapterItem,
        newItem: BaseAdapterItem
    ): Boolean {
        return newItem.matches(oldItem)
    }

    override fun areContentsTheSame(
        oldItem: BaseAdapterItem,
        newItem: BaseAdapterItem
    ): Boolean {
        return newItem.same(oldItem)
    }


}