package com.filip.randomcityapp.parsers

import android.graphics.Color
import androidx.core.graphics.ColorUtils
import arrow.core.None
import arrow.core.some
import java.util.*

object ColorParser {
    fun parseColorName(colorName: String) =
        try {
            Color.parseColor(colorName.toLowerCase(Locale.getDefault())).some()
        } catch (e: Exception) {
            None
        }

    fun isColorTooBright(color: Int) = ColorUtils.calculateLuminance(color) > 0.9
}