package com.filip.randomcityapp.parsers

import java.text.SimpleDateFormat
import java.util.*

object DateParser {
    private const val DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"
    fun convertMillisToDate(
        date: Long,
        outputFormat: String = DATE_TIME_FORMAT
    ): String {
        return SimpleDateFormat(outputFormat, Locale.getDefault()).format(Date(date))
    }
}