package com.filip.randomcityapp.parsers

import android.graphics.Color
import arrow.core.None
import arrow.core.some
import io.mockk.clearStaticMockk
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.After
import org.junit.Test

class ColorParserTest {

    private fun mockParser(desiredColor: Int, error: Boolean = false) {
        mockkStatic(Color::class)
        if (error) {
            every { Color.parseColor(any()) } throws RuntimeException()
        } else {
            every { Color.parseColor(any()) } returns desiredColor
        }
    }

    @After
    fun clear() {
        clearStaticMockk(Color::class)
    }

    @Test
    fun `test when known color name is passed THEN parser responds with Option-Some with corresponding color int`() {
        mockParser(Color.RED)
        assert(ColorParser.parseColorName("Red") == Color.RED.some())
    }

    @Test
    fun `test when unknown color name is passed THEN parser responds Option-None`() {
        mockParser(Color.RED, error = true)
        assert(ColorParser.parseColorName("Reddd") == None)
    }
}