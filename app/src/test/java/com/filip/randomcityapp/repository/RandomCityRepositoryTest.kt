package com.filip.randomcityapp.repository

import com.filip.randomcityapp.db.RandomCityDatabase
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.BehaviorSubject
import org.junit.After
import org.junit.Test
import java.util.concurrent.TimeUnit

class RandomCityRepositoryTest {

    private val disposable = CompositeDisposable()
    private val testScheduler = TestScheduler()
    private val lifecycleEventsSubject =
        BehaviorSubject.createDefault<LifecycleEvent>(
            LifecycleEvent.Foreground)

    private val database = mockk<RandomCityDatabase>()
    private val lifecycleNotifier = mockk<LifecycleNotifier>()

    private fun repository(): RandomCityRepository {
        every { database.putCity(any()) } returns Unit
        every { lifecycleNotifier.lifecycleEvents } returns lifecycleEventsSubject

        return RandomCityRepository(database, lifecycleNotifier, testScheduler)
    }

    @After
    fun clear() {
        disposable.clear()
    }

    @Test
    fun `when less than 5s passed THEN no items were emitted`() {
        val items = repository().randomCity
            .test()

        testScheduler.advanceTimeBy(4, TimeUnit.SECONDS)

        items.assertNoValues()
    }

    @Test
    fun `when 5s passed THEN exactly one item is emitted`() {
        val items = repository().randomCity
            .test()

        testScheduler.advanceTimeBy(5, TimeUnit.SECONDS)

        items.assertValueCount(1)
    }

    @Test
    fun `when 20s passed THEN 4 items were emitted`() {
        val items = repository().randomCity
            .test()

        testScheduler.advanceTimeBy(20, TimeUnit.SECONDS)

        items.assertValueCount(4)
    }

    @Test
    fun `when 4 items were emitted THEN each of them was saved to database`() {
        repository().randomCity.test()

        testScheduler.advanceTimeBy(20, TimeUnit.SECONDS)

        verify(exactly = 4) { database.putCity(any()) }
    }

    @Test
    fun `when app goes to background after 20s THEN no new items are emitted`() {
        val items = repository().randomCity
            .test()

        testScheduler.advanceTimeBy(20, TimeUnit.SECONDS)

        items.assertValueCount(4)

        lifecycleEventsSubject.onNext(LifecycleEvent.Background)

        testScheduler.advanceTimeBy(40, TimeUnit.SECONDS)

        items.assertValueCount(4)
    }

    @Test
    fun `when app goes to background after 20s and returns to foreground after 10s THEN emission is resumed and after 20s 4 new items are emitted`() {
        val items = repository().randomCity
            .test()

        testScheduler.advanceTimeBy(20, TimeUnit.SECONDS)

        items.assertValueCount(4)

        lifecycleEventsSubject.onNext(LifecycleEvent.Background)

        testScheduler.advanceTimeBy(10, TimeUnit.SECONDS)

        items.assertValueCount(4)

        lifecycleEventsSubject.onNext(LifecycleEvent.Foreground)

        testScheduler.advanceTimeBy(20, TimeUnit.SECONDS)

        items.assertValueCount(8)
    }
}