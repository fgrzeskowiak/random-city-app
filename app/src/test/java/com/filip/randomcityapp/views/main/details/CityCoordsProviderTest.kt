package com.filip.randomcityapp.views.main.details

import android.location.Address
import android.location.Geocoder
import arrow.core.None
import arrow.core.some
import com.google.android.gms.maps.model.LatLng
import io.mockk.every
import io.mockk.mockk
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import java.lang.RuntimeException

class CityCoordsProviderTest {
    private val testScheduler = TestScheduler()
    private val geocoder = mockk<Geocoder>()

    private fun mockProvider(
        latLng: Double,
        error: Boolean = false,
        emptyResult: Boolean = false
    ): CityCoordsProvider {
        val address = mockk<Address>().apply {
            every { latitude } returns latLng
            every { longitude } returns latLng
        }

        when {
            error -> every { geocoder.getFromLocationName(any(), any()) } throws RuntimeException()
            emptyResult -> every { geocoder.getFromLocationName(any(), any()) } returns emptyList()
            else -> every { geocoder.getFromLocationName(any(), any()) } returns listOf(address)
        }

        return CityCoordsProvider(geocoder, testScheduler)
    }

    @Test
    fun `when no item found for given city name THEN Option-None is emitted`() {

        val coords = mockProvider(0.0, emptyResult = true)
            .getCityCoords("City")
            .test()

        testScheduler.triggerActions()

        coords
            .assertValue(None)
            .assertValueCount(1)
    }

    @Test
    fun `when exception occured while processing city THEN Option-None is emitted`() {
        val coords = mockProvider(0.0, error = true)
            .getCityCoords("City")
            .test()

        testScheduler.triggerActions()

        coords
            .assertValue(None)
            .assertValueCount(1)
    }

    @Test
    fun `when city is properly parse THEN Option-Some with coords is emitted`() {
        val latLng = 52.0

        val coords = mockProvider(latLng)
            .getCityCoords("City")
            .test()

        testScheduler.triggerActions()

        coords
            .assertValue(LatLng(latLng, latLng).some())
            .assertValueCount(1)
    }
}