package com.filip.randomcityapp.views.splash

import com.filip.randomcityapp.repository.RandomCity
import com.filip.randomcityapp.repository.RandomCityRepository
import com.filip.randomcityapp.views.main.generateCities
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Test

class SplashViewModelTest {
    private val testScheduler = TestScheduler()
    private val repository = mockk<RandomCityRepository>()

    private fun mockViewModel(cities: List<RandomCity>, error: Boolean = false): SplashViewModel {
        every { repository.randomCity } returns Observable.fromIterable(cities)

        return SplashViewModel(repository, testScheduler)
    }

    @Test
    fun `when no city is emitted yet THEN no values present`() {
        val cityEvent = mockViewModel(emptyList())
            .randomCityGenerated
            .test()

        testScheduler.triggerActions()

        cityEvent.assertNoValues()
    }

    @Test
    fun `when city is emitted THEN event is generated`() {
        val items = mockViewModel(generateCities(1))
            .randomCityGenerated
            .test()

        testScheduler.triggerActions()

        items.assertValueCount(1)
    }

    @Test
    fun `when 3 cities are emitted THEN only one event is generated`() {
        val items = mockViewModel(generateCities(3))
            .randomCityGenerated
            .test()

        testScheduler.triggerActions()

        items.assertValueCount(1)
    }
}