package com.filip.randomcityapp.views.main

import com.filip.randomcityapp.repository.RandomCity

fun generateCities(count: Int) = (0 until count)
    .map { index ->
        RandomCity(
            "City$index",
            "Color$index",
            System.currentTimeMillis()
        )
    }