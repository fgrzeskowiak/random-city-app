package com.filip.randomcityapp.views.main

import android.graphics.Color
import arrow.core.None
import arrow.core.some
import com.filip.randomcityapp.parsers.ColorParser
import com.filip.randomcityapp.repository.RandomCity
import com.filip.randomcityapp.repository.RandomCityRepository
import com.filip.randomcityapp.views.main.details.CityCoordsProvider
import com.google.android.gms.maps.model.LatLng
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.unmockkObject
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Test
import kotlin.random.Random

class MainViewModelTest {

    private val disposable = CompositeDisposable()
    private val testScheduler = TestScheduler()
    private val repository = mockk<RandomCityRepository>()
    private val coordsProvider = mockk<CityCoordsProvider>()
    private val randomGenerator = Random(111)

    @After
    fun clear() {
        disposable.clear()
    }

    private fun mockViewModel(cities: List<RandomCity>, error: Boolean = false): MainViewModel {
        every { repository.randomCity } returns Observable.fromIterable(cities)
        every { coordsProvider.getCityCoords(any()) } answers {
            Observable.just(
                if (error) {
                    None
                } else {
                    LatLng(randomGenerator.nextDouble(), randomGenerator.nextDouble()).some()
                }
            )
        }

        return MainViewModel(repository, testScheduler, coordsProvider)
    }

    @Test
    fun `when no cities are emitted THEN only empty list is emitted`() {
        val items = mockViewModel(emptyList()).cityItems
            .test()

        testScheduler.triggerActions()

        items
            .assertValue { it.isEmpty() }
            .assertValueCount(1)
    }

    @Test
    fun `when 3 cities are emitted THEN empty list and 3 list updates are emitted`() {
        val items = mockViewModel(generateCities(3)).cityItems
            .test()

        testScheduler.triggerActions()

        items
            .assertValueAt(0) { it.isEmpty() }
            .assertValueCount(4)
    }

    @Test
    fun `when cities are emitted not in alphabetical order THEN each list is properly sorted by names`() {
        val reversedAlphabeticalList = generateCities(4).reversed()
        val items = mockViewModel(reversedAlphabeticalList).cityItems
            .map { it.map { cityItem -> (cityItem as CityItem).city } }
            .test()

        testScheduler.triggerActions()

        items
            .assertValueAt(0, emptyList())
            .assertValueAt(1, listOf("City3"))
            .assertValueAt(2, listOf("City2", "City3"))
            .assertValueAt(3, listOf("City1", "City2", "City3"))
            .assertValueAt(4, listOf("City0", "City1", "City2", "City3"))
    }

    @Test
    fun `when items with the same name are emitted THEN each list is sorted both by name and by smallest timestamp`() {
        val firstCity = RandomCity(
            "City1",
            "Color",
            System.currentTimeMillis()
        )
        val secondCity = RandomCity(
            "City2",
            "Color",
            System.currentTimeMillis() + 100
        )
        val thirdCity = RandomCity(
            "City1",
            "Color",
            System.currentTimeMillis() + 200
        )
        val items = mockViewModel(listOf(firstCity, secondCity, thirdCity)).cityItems
            .map { it.map { cityItem -> (cityItem as CityItem).timestamp } }
            .test()

        testScheduler.triggerActions()

        items
            .assertValueAt(0, emptyList())
            .assertValueAt(1, listOf(firstCity.timestamp))
            .assertValueAt(2, listOf(firstCity.timestamp, secondCity.timestamp))
            .assertValueAt(
                3,
                listOf(firstCity.timestamp, thirdCity.timestamp, secondCity.timestamp)
            )
    }

    @Test
    fun `when each of two cities from the list are selected THEN two opening actions are emitted without duplicates`() {
        val viewModel = mockViewModel(generateCities(2))
        val items = viewModel.cityItems

        disposable.add(
            items
                .doOnNext { it.forEach { item -> (item as CityItem).citySelectedObserver.onNext(item) } }
                .subscribe()
        )

        val openAction = viewModel.openCity
            .test()

        testScheduler.triggerActions()

        openAction
            .assertValueCount(2)
    }

    @Test
    fun `when the same city is selected three times THEN only one open action is emitted`() {
        val viewModel = mockViewModel(generateCities(3))
        val items = viewModel.cityItems

        disposable.add(
            items
                .doOnNext {
                    (it.firstOrNull() as? CityItem)?.let { cityItem ->
                        cityItem.citySelectedObserver.onNext(cityItem)
                    }
                }
                .subscribe()
        )

        val openAction = viewModel.openCity
            .test()

        testScheduler.triggerActions()

        openAction
            .assertValueCount(1)
    }

    @Test
    fun `when city is selected THEN toolbar is updated with proper color`() {
        val desiredColor = Color.RED
        val viewModel = mockViewModel(listOf(
            RandomCity(
                "City",
                "Red",
                System.currentTimeMillis()
            )
        ))
        val items = viewModel.cityItems

        mockkObject(ColorParser)
        every { ColorParser.parseColorName(any()) } returns (desiredColor.some())

        val toolbarUpdate = viewModel.toolbarUpdate

        disposable.addAll(
            items
                .doOnNext { it.forEach { item -> (item as CityItem).citySelectedObserver.onNext(item) } }
                .subscribe()
        )

        testScheduler.triggerActions()

        unmockkObject(ColorParser)

        toolbarUpdate
            .test()
            .assertValue { (it is ToolbarUpdate.CityColor) && it.color == desiredColor }
            .assertValueCount(1)
    }

    @Test
    fun `when back button is clicked THEN toolbar goes back to default setup`() {
        val viewModel = mockViewModel(generateCities(4))

        val toolbarUpdate = viewModel.toolbarUpdate

        viewModel.backClicked()

        testScheduler.triggerActions()

        toolbarUpdate
            .test()
            .assertValue(ToolbarUpdate.Default)
            .assertValueCount(1)
    }

    @Test
    fun `when each item is selected THEN both always Loading and Loaded events are emitted`() {
        val viewModel = mockViewModel(generateCities(4))
        val items = viewModel.cityItems

        disposable.addAll(
            items
                .lastOrError()
                .doOnSuccess { it.forEach { (it as CityItem).citySelectedObserver.onNext(it) } }
                .subscribe()
        )

        val map = viewModel.withMap(Observable.just(mockk()))
            .test()

        testScheduler.triggerActions()

        map
            .assertValueAt(0, CityDetailsState.Loading)
            .assertValueAt(1) { it is CityDetailsState.Loaded }
            .assertValueAt(2, CityDetailsState.Loading)
            .assertValueAt(3) { it is CityDetailsState.Loaded }
            .assertValueAt(4, CityDetailsState.Loading)
            .assertValueAt(5) { it is CityDetailsState.Loaded }
            .assertValueAt(6, CityDetailsState.Loading)
            .assertValueAt(7) { it is CityDetailsState.Loaded }
            .assertValueCount(8)
    }

    @Test
    fun `when each item is selected but coords provider fails THEN Loading and Error are emitted for every item`() {
        val viewModel = mockViewModel(generateCities(4), error = true)
        val items = viewModel.cityItems

        disposable.addAll(
            items
                .lastOrError()
                .doOnSuccess { it.forEach { (it as CityItem).citySelectedObserver.onNext(it) } }
                .subscribe()
        )

        val map = viewModel.withMap(Observable.just(mockk()))
            .test()

        testScheduler.triggerActions()

        map
            .assertValueAt(0, CityDetailsState.Loading)
            .assertValueAt(1, CityDetailsState.Error)
            .assertValueAt(2, CityDetailsState.Loading)
            .assertValueAt(3, CityDetailsState.Error)
            .assertValueAt(4, CityDetailsState.Loading)
            .assertValueAt(5, CityDetailsState.Error)
            .assertValueAt(6, CityDetailsState.Loading)
            .assertValueAt(7, CityDetailsState.Error)
            .assertValueCount(8)
    }
}