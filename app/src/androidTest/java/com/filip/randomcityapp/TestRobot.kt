package com.filip.randomcityapp

import android.app.Activity
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import com.filip.randomcityapp.views.main.list.CityViewHolderManager
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.`is`
import kotlin.reflect.KClass


fun actionRobot(func: (ActionRobot.() -> Unit)) = ActionRobot().apply(func)

class ActionRobot {
    fun clickRecycleViewItem(@IdRes viewId: Int, position: Int) {
        onView(withId(viewId))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<CityViewHolderManager.ViewHolder>(
                    position,
                    ViewActions.click()
                )
            )
    }

    fun rotateActivity(activity: Activity, destinationOrientation: Int) {
        activity.requestedOrientation = destinationOrientation
    }

    fun waitFor(millis: Long) {
        Thread.sleep(millis)
    }

    fun withoutWaiting(func: (ResultRobot.() -> Unit)): ResultRobot {
        return ResultRobot().apply(func)
    }

    fun waitForItemEmission(func: (ResultRobot.() -> Unit)): ResultRobot {
        Thread.sleep(6000)
        return ResultRobot().apply(func)
    }
}

class ResultRobot {
    fun isTextVisible(@IdRes viewId: Int, @StringRes textRes: Int) {
        onView(withId(viewId))
            .check(matches(withText(textRes)))
    }

    fun isViewVisible(@IdRes viewId: Int) {
        onView(withId(viewId))
            .check(matches(isDisplayed()))
    }

    fun isActivityOpened(kClass: KClass<*>) {
        intended(IntentMatchers.hasComponent(kClass.java.name))
    }

    fun isViewInvisible(@IdRes viewId: Int) {
        onView(withId(viewId)).check(doesNotExist())
    }

    fun isToolbarTitleChanged(@IdRes toolbarId: Int) {
        onView(withId(toolbarId)).check(matches(hasDescendant(not(withText(R.string.app_name)))))
    }

    fun isRecyclerViewSize(@IdRes viewId: Int, count: Int) {
        onView(withId(viewId)).check(RecyclerViewItemCountAssertion(count))
    }

    class RecyclerViewItemCountAssertion(private val expectedCount: Int) : ViewAssertion {
        override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
            if (noViewFoundException != null) {
                throw noViewFoundException
            }
            val adapter = (view as RecyclerView).adapter
            assertThat(adapter?.itemCount, `is`(expectedCount))
        }

    }
}

fun withIntents(func: (() -> Unit)) {
    Intents.init()
    func()
    Intents.release()
}