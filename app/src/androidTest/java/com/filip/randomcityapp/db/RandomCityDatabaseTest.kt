package com.filip.randomcityapp.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RandomCityDatabaseTest {

    private lateinit var database: RandomCityDb

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            RandomCityDb::class.java
        ).build()
    }

    @After
    fun closeDb() {
        database.clearAllTables()
        database.close()
    }

    @Test
    fun whenCityIsInserted_itIsAvailableById() {
        database.clearAllTables()
        val timestamp = System.currentTimeMillis()
        val dao = database.cityWithColorDatabaseDao()
        dao.put(randomCity(timestamp))

        dao.get(timestamp)
            .test()
            .assertValueCount(1)
    }

    @Test
    fun whenCityIsInserted_itIsAvailableOnList() {
        database.clearAllTables()
        val dao = database.cityWithColorDatabaseDao()
        dao.put(randomCity())

        dao.getAll()
            .test()
            .assertValue { it.isNotEmpty() }
    }

    @Test
    fun when10CitiesAreInserted_allAreAvailableOnList() {
        database.clearAllTables()
        val dao = database.cityWithColorDatabaseDao()

        repeat(10) {
            dao.put(randomCity(id = it))
        }

        dao.getAll()
            .test()
            .assertValue { it.size == 10 }
    }

    @Test
    fun whenCityIsInsertedAndDatabaseIsCleared_returnedListIsEmpty() {
        database.clearAllTables()
        val dao = database.cityWithColorDatabaseDao()
        dao.put(randomCity())
        dao.clearDb()

        dao.getAll()
            .test()
            .assertValue { it.isEmpty() }
    }

    @Test
    fun whenTwoCitiesWithTheSameTimestampAreInserted_onlyTheSecondRemainsInDatabase() {
        database.clearAllTables()
        val timestamp = System.currentTimeMillis()
        val firstCity = RandomCityEntity(timestamp, "City1", "color")
        val secondCity = RandomCityEntity(timestamp, "City2", "color")

        val dao = database.cityWithColorDatabaseDao()
        dao.put(firstCity)
        dao.put(secondCity)

        dao.getAll()
            .test()
            .assertValue { it.size == 1 && it.contains(secondCity) }
            .assertValueCount(1)
    }

    private fun randomCity(timestamp: Long? = null, id: Int = 0) =
        RandomCityEntity(timestamp ?: System.currentTimeMillis(), "City$id", "color$id")
}