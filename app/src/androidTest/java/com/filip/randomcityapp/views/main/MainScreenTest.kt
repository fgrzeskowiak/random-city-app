package com.filip.randomcityapp.views.main

import android.content.pm.ActivityInfo
import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.filip.randomcityapp.R
import com.filip.randomcityapp.actionRobot
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainScreenTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun whenCityIsSelected_mapIsShown() {
        actionRobot {
            waitForItemEmission {
                isViewVisible(R.id.cities_recycler_view)
                clickRecycleViewItem(R.id.cities_recycler_view, 0)
                isViewInvisible(R.id.cities_recycler_view)
                isViewVisible(R.id.map_view)
            }
        }
    }

    @Test
    fun whenCityIsSelectedAndScreenIsRotated_mapIsStillVisible() {
        actionRobot {
            waitForItemEmission {
                clickRecycleViewItem(R.id.cities_recycler_view, 0)
                isViewVisible(R.id.map_view)
                rotateActivity(rule.activity, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                waitFor(500)
                isViewVisible(R.id.map_view)
            }
        }
    }

    @Test
    fun whenCityIsSelectedAndScreenIsRotatedAndBackIsClicked_goBackToList() {
        actionRobot {
            waitForItemEmission {
                isViewVisible(R.id.cities_recycler_view)
                clickRecycleViewItem(R.id.cities_recycler_view, 0)
                isViewVisible(R.id.map_view)
                rotateActivity(rule.activity, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
                waitFor(500)
                isViewVisible(R.id.map_view)
                pressBack()
                isViewVisible(R.id.cities_recycler_view)
            }
        }
    }

    @Test
    fun whenCityIsSelected_toolbarTitleIsChanged() {
        actionRobot {
            waitForItemEmission {
                clickRecycleViewItem(R.id.cities_recycler_view, 0)
                isToolbarTitleChanged(R.id.main_toolbar)
            }
        }
    }

    @Test
    fun whenItemIsEmittedAndTenMoreSecondsPasses_listHasThreeItems() {
        actionRobot {
            waitForItemEmission {
                waitFor(10000)
                isRecyclerViewSize(R.id.cities_recycler_view, 3)
            }
        }
    }

    @Test
    fun whenMapIsOpenedAndTenMoreSecondsPassesAndBackIsClicked_listIsUpdatedInBackground() {
        actionRobot {
            waitForItemEmission {
                clickRecycleViewItem(R.id.cities_recycler_view, 0)
                waitFor(10000)
                pressBack()
                isRecyclerViewSize(R.id.cities_recycler_view, 3)
            }
        }
    }
}