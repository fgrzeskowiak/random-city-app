package com.filip.randomcityapp.views.splash

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.rule.ActivityTestRule
import com.filip.randomcityapp.R
import com.filip.randomcityapp.actionRobot
import com.filip.randomcityapp.views.main.MainActivity
import com.filip.randomcityapp.withIntents
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class SplashScreenTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun appNameAndProgress_areDisplayed() {
        actionRobot {
            withoutWaiting {
                isTextVisible(R.id.splash_text, R.string.app_name)
                isViewVisible(R.id.splash_progress)
            }
        }
    }

    @Test
    fun afterItemEmission_mainActivityIsOpened() {
        withIntents {
            actionRobot {
                waitForItemEmission {
                    isActivityOpened(MainActivity::class)
                }
            }
        }
    }

    @Test
    fun evenIfSplashIsRotatedFiveTimes_mainActivityIsOpenedAfterFiveSeconds() {
        withIntents {
            actionRobot {
                withoutWaiting {
                    repeat(5) {
                        rotateActivity(rule.activity, it % 2)
                        waitFor(1000)
                    }
                    isActivityOpened(MainActivity::class)
                }
            }
        }
    }
}