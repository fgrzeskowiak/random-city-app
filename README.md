# Random City App

## Running the app

In order to run the app, please create a file called __api.keys__ in the root folder of the project and put there a value: **mapApiKey=API_KEY**.
It is necessary to display the maps correctly.
This key must be generated from a new project on Google Cloud Platform, which has Maps SDK for Android enabled.

## Structure

This application is organized into following packages, representing different functionalities.

### Common

Includes different utility classes and extension functions shared across the app logic.

### Dagger

This package contains classes and interfaces used by Dagger framework to preform Dependency Injection.

### DB

Contains Room classes and interfaces needed to store emitted cities on the device.

### Parsers

Contains utility objects used for parsing colors and dates.

### Repository

Includes a RandomCityRepository class responsible for generating cities every 5 seconds and a LifecycleNotifier class, which produces events when app goes to background or to foreground.
It is used to pause and resume the emission when the app is not visible, because only unsubscribing from the producer would also restart the emission timer on screen rotation.

### Views

Contains Activities, Fragments and ViewModels which build the presentation layer.
There's also a CityCoordsProvider class there, which is a reactive wrapper for the Geocoder.

## Tests

Both Unit and UI tests were added to to application. UI tests utilize the Robot pattern for better readability and maintenance.

## Additional Info

App supports dark mode.
In order to properly show bright colors on the list, like White or Yellow, small shadow is added to the TextView when such color is emitted.
Also, when toolbar color changes to one of those two colors, its title is turned to black to keep good contrast. It was achieved by calculating color luminance.